# Poker

A Simple poker game

## Get Started

```bash
git clone https://github.com/digitaldesigndj/javascript-video-poker poker
cd poker
npm install
docpad run
```

Then visit [http://localhost:9779](http://localhost:9779)

## Keyboard Controls

Enter to deal/start game, 1-5 to hold.

## License

&copy; 2013 Taylor Young

MIT Licensed
